## CREATE DATABASE theater;
USE theater;
DROP TABLE IF EXISTS Movie ;
CREATE TABLE Movie (
    id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(255) NOT NULL,
    year INT NOT NULL,
    duration INT NOT NULL,
    country VARCHAR(255) NOT NULL,
    language VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL,
    genre VARCHAR(255) NOT NULL,
    image VARCHAR(255) NOT NULL,
    cert VARCHAR(255) NOT NULL,
    director VARCHAR(255) NOT NULL,
    stars VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);