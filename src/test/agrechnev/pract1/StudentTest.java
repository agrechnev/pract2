package agrechnev.pract1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by student on 7/12/2016.
 */
public class StudentTest {
  @Test
  public void test1(){
      Student s=new Student("Mickey","Mouse",new Student.Group(1,"elemental magic"));

      Student.Exam e1=new Student.Exam("fire magic",10,2016,1);
      Student.Exam e2=new Student.Exam("water magic",10,2016,1);
      Student.Exam e3=new Student.Exam("air magic",8,2016,1);
      Student.Exam e4=new Student.Exam("earth magic",10,2015,2);

      s.addExam(e1);
      s.addExam(e2);
      s.addExam(e3);
      s.addExam(e4);

      assertEquals(s.numWithGrade(10),3);
      assertEquals(s.averageGrade(2016,1),28.0d/3,0.000001d);

      try {
          s.removeExam(new Student.Exam("water magic", 10, 2016, 1));
      } catch (Student.StudentException se) {fail();}

      assertEquals(s.numWithGrade(10),2);
      assertEquals(s.averageGrade(2016,1),9.0d,0.000001d);
  }

  @Test(expected=Student.StudentException.class)
  public void test2() throws Student.StudentException{
      Student s=new Student("Mickey","Mouse",new Student.Group(1,"elemental magic"));

      Student.Exam e1=new Student.Exam("fire magic",10,2016,1);
      Student.Exam e2=new Student.Exam("water magic",10,2016,1);
      Student.Exam e3=new Student.Exam("air magic",8,2016,1);
      Student.Exam e4=new Student.Exam("earth magic",10,2015,2);

      s.addExam(e1);
      s.addExam(e2);
      s.addExam(e3);
      s.addExam(e4);

      s.removeExam(new Student.Exam("necromancy", 10, 2015, 2));
  }

    @Test
    public void test3(){
        Student s=new Student("Mickey","Mouse",new Student.Group(1,"elemental magic"));

        Student.Exam e1=new Student.Exam("fire magic",5,2015,1);
        Student.Exam e2=new Student.Exam("fire magic",10,2015,2);
        Student.Exam e3=new Student.Exam("fire magic",8,2016,1);

        s.addExam(e1);
        s.addExam(e2);
        s.addExam(e3);

        assertEquals(s.highestGrade("fire magic"),10);
    }
}