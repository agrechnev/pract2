package agrechnev.pract1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ArrayProdTest{
    @Test
    public void testProd()  throws ArrayProd.NullArrayException{
        int[] arr={1,2,3,4};

        assertEquals(ArrayProd.Prod(arr),24);
    }
}