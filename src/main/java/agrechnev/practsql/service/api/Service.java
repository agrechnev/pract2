package agrechnev.practsql.service.api;

import agrechnev.practsql.dto.EntityDTO;

import java.util.List;

/**
 * Created by Oleksiy Grechnyev on 9/10/2016.
 * Interface for a Service classes which must implement CRUD
 * K is the key type (Integer or Long)
 * T is the DTO type (i.e. MovieDTO)
 */
public interface Service<K extends Number,T extends EntityDTO<K>> {
    static class ServException extends Exception {
        public ServException(String message, Throwable cause) {
            super(message, cause);
        }
    };


    // Get all dtos as a list
    List<T> getAll() throws ServException;

    // Get a dto by ID or null if not found
    T get(K id) throws ServException;

    // The following methods cast ServException if unsuccessfull

    // Delete a dto by ID
    void delete(K id) throws ServException;

    // Delete all DTO's
    void deleteAll() throws ServException;

    // Add a dto and return ID
    // Also sets ID in the dto object
    K add(T dto) throws ServException;

    // Update a dto (must have a correct ID)
    void update(T dto) throws ServException;

    // Load all objects linked to this one (only 1 step) into memory
    default void loadLinked(T dto) throws ServException{
        // Do nothing
    }
}
