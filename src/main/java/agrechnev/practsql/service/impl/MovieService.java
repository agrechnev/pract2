package agrechnev.practsql.service.impl;

import agrechnev.practsql.dao.DaoFactory;
import agrechnev.practsql.dao.api.Dao;
import agrechnev.practsql.dto.MovieDTO;
import agrechnev.practsql.mapper.BeanMapper;
import agrechnev.practsql.model.Movie;
import agrechnev.practsql.service.api.Service;

import java.util.List;

/**
 * Created by Oleksiy Grechnyev on 9/13/2016.
 * Implementation of Service for any DTO
 * Uses BeanMappper to map
 */
public class MovieService implements Service<Integer, MovieDTO> {

    static MovieService movieServiceInstance=null;

    public static MovieService getInstance(){
        if (movieServiceInstance == null) movieServiceInstance=new MovieService();

        return movieServiceInstance;
    }

    @Override
    public List<MovieDTO> getAll() throws ServException {
        try {
            List<Movie> mList= DaoFactory.getInstance().getMovieDao().getAll();
            return BeanMapper.getInstance().mapCollection(mList,MovieDTO.class);

        } catch (Dao.DaoException e) {
            throw new ServException("MovieService.getAll: caught a Daoexception",e);
        }
    }

    @Override
    public MovieDTO get(Integer id) throws ServException {
        try {
            Movie movie=DaoFactory.getInstance().getMovieDao().get(id);

            return BeanMapper.getInstance().map(movie,MovieDTO.class);
        } catch (Dao.DaoException e) {
            throw new ServException("MovieService.get: caught a Daoexception",e);
        }

    }

    @Override
    public void delete(Integer id) throws ServException {
        try {
            DaoFactory.getInstance().getMovieDao().delete(id);
        } catch (Dao.DaoException e) {
            throw new ServException("MovieService.delete: caught a Daoexception",e);
        }
    }

    // Delete all DTOs
    @Override
    public void deleteAll() throws ServException {
        try {
            DaoFactory.getInstance().getMovieDao().deleteAll();
        } catch (Dao.DaoException e) {
            throw new ServException("MovieService.delete: caught a Daoexception",e);
        }
    }

    @Override
    public Integer add(MovieDTO dto) throws ServException {
        Movie movie=BeanMapper.getInstance().map(dto,Movie.class);

        try {
            return DaoFactory.getInstance().getMovieDao().add(movie);
        } catch (Dao.DaoException e) {
            throw new ServException("MovieService.add: caught a Daoexception",e);
        }
    }

    @Override
    public void update(MovieDTO dto) throws ServException {

        Movie movie=BeanMapper.getInstance().map(dto,Movie.class);

        try {
            DaoFactory.getInstance().getMovieDao().update(movie);
        } catch (Dao.DaoException e) {
            throw new ServException("MovieService.update: caught a Daoexception",e);
        }
    }
}
