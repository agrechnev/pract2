package agrechnev.practsql.model;

/**
 * Created by Oleksiy Grechnyev on 8/12/2016.
 * A bean class for Movie
 */
public class Movie extends Entity<Integer> {
    private static final long serialVersionUID = -6822203991510365114L;

    private String title; // Movie title
    private int year;  // Movie relese year
    private int duration;   // Duration in min
    private String country;
    private String language;
    private String description;
    private String genre;
    private String image; // Movie poster file or URL
    private String cert;  // Movie certification
    private String director; // Movie director
    private String stars; // Main actors

    public Movie(String title, int year, int duration, String country, String language, String description, String genre, String image, String cert, String director, String stars) {
        this.title = title;
        this.year = year;
        this.duration = duration;
        this.country = country;
        this.language = language;
        this.description = description;
        this.genre = genre;
        this.image = image;
        this.cert = cert;
        this.director = director;
        this.stars = stars;
    }

    public Movie() {
    }


    // Getters+setters
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCert() {
        return cert;
    }

    public void setCert(String cert) {
        this.cert = cert;
    }

    public String getDirector() { return director; }

    public void setDirector(String director) { this.director = director; }

    public String getStars() { return stars; }

    public void setStars(String stars) { this.stars = stars; }

    @Override
    public String toString() {
        return "Movie{" +
                "id="+id+
                ", title='" + title + '\'' +
                ", year=" + year +
                ", duration=" + duration +
                ", country='" + country + '\'' +
                ", language='" + language + '\'' +
                ", description='" + description + '\'' +
                ", genre='" + genre + '\'' +
                ", image='" + image + '\'' +
                ", cert='" + cert + '\'' +
                ", director='" + director + '\'' +
                ", stars='" + stars + '\'' +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Movie movie = (Movie) o;

        if (year != movie.year) return false;
        if (duration != movie.duration) return false;
        if (title != null ? !title.equals(movie.title) : movie.title != null) return false;
        if (country != null ? !country.equals(movie.country) : movie.country != null) return false;
        if (language != null ? !language.equals(movie.language) : movie.language != null) return false;
        if (description != null ? !description.equals(movie.description) : movie.description != null) return false;
        if (genre != null ? !genre.equals(movie.genre) : movie.genre != null) return false;
        if (image != null ? !image.equals(movie.image) : movie.image != null) return false;
        if (cert != null ? !cert.equals(movie.cert) : movie.cert != null) return false;
        if (director != null ? !director.equals(movie.director) : movie.director != null) return false;
        return stars != null ? stars.equals(movie.stars) : movie.stars == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + year;
        result = 31 * result + duration;
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (language != null ? language.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (genre != null ? genre.hashCode() : 0);
        result = 31 * result + (image != null ? image.hashCode() : 0);
        result = 31 * result + (cert != null ? cert.hashCode() : 0);
        result = 31 * result + (director != null ? director.hashCode() : 0);
        result = 31 * result + (stars != null ? stars.hashCode() : 0);
        return result;
    }
}
