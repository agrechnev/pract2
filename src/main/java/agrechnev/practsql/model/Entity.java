package agrechnev.practsql.model;

import java.io.Serializable;

/**
 * Created by Oleksiy Grechnyev on 8/12/2016.
 * A parent class for beans like Movie, Ticket, User etc
 * Implements one private field id
 * I want to make it cloneable for the sake of inMemoryDB
 */
public abstract class Entity<K extends Number> implements Serializable, Cloneable {
    private static final long serialVersionUID = -5318676765251480426L;
    protected K id;

    public Entity() {
    }

    public Entity(K id) {
        this.id = id;
    }

    public K getId() {
        return id;
    }

    public void setId(K id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Entity<?> entity = (Entity<?>) o;

        return id != null ? id.equals(entity.id) : entity.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Entity{" +
                "id=" + id +
                '}';
    }


    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
