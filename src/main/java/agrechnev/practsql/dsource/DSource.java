package agrechnev.practsql.dsource;

import agrechnev.practsql.helpers.TheaterConfig;
import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Oleksiy Grechnyev on 9/11/2016.
 * My data source which provides a Connection when needed
 * A little singleton wrapper for the C3P0 ComboPooledDataSource in fact
 */
public class DSource {
    // My exception class with constructors
    public static class DSourceException extends Exception{
        public DSourceException(String message) {
            super(message);
        }

        public DSourceException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    private static DSource dSourceInstance=null; // The singleton instance

    // The C3P0 datasource
    private ComboPooledDataSource cpds;

    // Private constructor
    private DSource() throws DSourceException {
        initDSource();
    }

    // Initialize the data source with C3P0
    private void initDSource() throws DSourceException {
        // Initialize (if needed) and get the instance of TheaterConfig
        TheaterConfig theaterConfig=TheaterConfig.getInstance();

        // Create the c3p0 pool
        cpds=new ComboPooledDataSource();

        try {
            cpds.setDriverClass( theaterConfig.getDbDriver() ); //loads the jdbc driver
            cpds.setJdbcUrl(theaterConfig.getDbURL());
            cpds.setUser(theaterConfig.getDbUser());
            cpds.setPassword(theaterConfig.getDbPassword());

            // the settings below are optional -- c3p0 can work with defaults
            cpds.setMinPoolSize(5);
            cpds.setAcquireIncrement(5);
            cpds.setMaxPoolSize(100);
            // The DataSource cpds is now a fully configured and usable pooled DataSource
        } catch (PropertyVetoException e) {
            throw new DSourceException(
                    "DSource.initDSource: c3p0 threw a PropertyVetoException.",e);
        }


    }

    // Get the instance of DSource
    public static DSource getInstance() throws DSourceException {
        if (dSourceInstance==null) dSourceInstance=new DSource();

        return dSourceInstance;
    }

    public Connection getConnection() throws SQLException {
         return cpds.getConnection();
    }
}
