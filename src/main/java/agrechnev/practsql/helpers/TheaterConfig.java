package agrechnev.practsql.helpers;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by Oleksiy Grechnyev on 9/11/2016.
 * Movie theater configuration singleton class
 */
public class TheaterConfig {
    // The single instance
    private static TheaterConfig theaterConfigInstance=null;

    // The properties
    private boolean inMemoryDB; // Use inMemoryDB instead of SQL?
    private String dbURL;
    private String dbUser;
    private String dbPassword;
    private String dbDriver;


    // Private constructor
    private TheaterConfig() {
        loadConfig();
    }

    // Load config from file
    private void loadConfig() {
        Properties prop=new Properties();


        try {
            prop.load(TheaterConfig.class.getClassLoader().getResourceAsStream("application.properties"));
            inMemoryDB=Boolean.parseBoolean(prop.getProperty("inMemoryDB"));
            dbURL=prop.getProperty("dbURL"); // Set up the properties
            dbUser=prop.getProperty("dbUser");
            dbPassword=prop.getProperty("dbPassword");
            dbDriver=prop.getProperty("dbDriver");

        } catch (IOException e) {
            System.out.println("Error! Cannot read configuration file application.properties");
            e.printStackTrace();
            System.exit(1); // No need to continue after that
        }
    }

    // The factory getInstance method
    public static TheaterConfig getInstance(){
        if (theaterConfigInstance == null) theaterConfigInstance=new TheaterConfig();

        return theaterConfigInstance;
    }

    //Getters

    public String getDbURL() {
        return dbURL;
    }

    public String getDbUser() {
        return dbUser;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public String getDbDriver() {
        return dbDriver;
    }

    public boolean getInMemoryDB() { return inMemoryDB; }
}
