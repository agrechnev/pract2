package agrechnev.practsql.mapper;

import org.dozer.DozerBeanMapper;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Oleksiy Grechnyev on 9/13/2016.
 * A signletom wrapper to DozerBeanMapper
 */
public class BeanMapper {
    static BeanMapper beanMapperInstance=null;

    DozerBeanMapper mapper;

    private BeanMapper() {
        mapper=new DozerBeanMapper();
    }

    public static BeanMapper getInstance(){
        if (beanMapperInstance == null) beanMapperInstance=new BeanMapper();

        return beanMapperInstance;
    }

    // Map a class with Dozer
    public <D> D map(Object source, Class<D> dest) {
        return mapper.<D>map(source,dest);
    }

    // Map a collection to a list
    public <S,D> List<D> mapCollection(Collection<S> in, Class<D> dest){
        return in.stream().map(s->mapper.<D>map(s,dest)).collect(Collectors.toList());
    }
}
