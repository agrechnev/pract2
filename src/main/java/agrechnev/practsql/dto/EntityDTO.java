package agrechnev.practsql.dto;

/**
 * Created by Oleksiy Grechnyev on 8/12/2016.
 * A parent class for DTOs like MovieDTO, TicketDTO, UserDTO etc
 * Implements one private field id
 * The DTO version of Entity
 */
public abstract class EntityDTO<K extends Number> {

    protected K id;

    public EntityDTO() {
    }

    public EntityDTO(K id) {
        this.id = id;
    }

    public K getId() {
        return id;
    }

    public void setId(K id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EntityDTO<?> entity = (EntityDTO<?>) o;

        return id != null ? id.equals(entity.id) : entity.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Entity{" +
                "id=" + id +
                '}';
    }

}
