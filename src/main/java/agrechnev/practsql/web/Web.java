package agrechnev.practsql.web;


import agrechnev.practsql.dsource.DSource;
import agrechnev.practsql.dto.MovieDTO;
import agrechnev.practsql.service.api.Service;
import agrechnev.practsql.service.impl.MovieService;

import java.sql.SQLException;

/**
 * Created by Oleksiy Grechnyev on 9/10/2016.
 * A dummy main program for testing
 */
public class Web {
    public static void main(String[] args) throws SQLException, DSource.DSourceException, Service.ServException{


        MovieDTO movie_Elektra=new MovieDTO("Elektra",2005,97,"Canada, USA","English","No description",
                "Action, Crime, Fantasy, Thriller", "No Image","PG-13","D1","S1");

        MovieDTO movie_PitchBlack=new MovieDTO("Pitch Black",2000,109,"USA","English","No description",
                "Horror, Sci-Fi","No Image","R","D2","S2");

//        Collections.list(DriverManager.getDrivers()).forEach(System.out::println);


        Service<Integer,MovieDTO> ms= MovieService.getInstance();

//        System.out.println("!!!!!!!!!! INITIALIZATION FINISHED !!!!!!!!!!!!!!!");


        System.out.println(ms.add(movie_Elektra));
        System.out.println(ms.add(movie_PitchBlack));
        System.out.println(ms.add(movie_Elektra));
        System.out.println(ms.add(movie_PitchBlack));
        System.out.println(ms.add(movie_Elektra));
        System.out.println(ms.add(movie_PitchBlack));
        System.out.println(ms.add(movie_Elektra));
        System.out.println(ms.add(movie_PitchBlack));
        System.out.println(ms.add(movie_Elektra));
        System.out.println(ms.add(movie_PitchBlack));
        System.out.println(ms.get(9));
        ms.delete(3);


        movie_PitchBlack.setId(7);
        ms.update(movie_PitchBlack);

        ms.getAll().forEach(System.out::println);
    }


}
