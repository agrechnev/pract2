package agrechnev.practsql.dao.impl;

import agrechnev.practsql.model.Movie;

import java.sql.*;

/**
 * Created by Oleksiy Grechnyev on 9/11/2016.
 * Movie version of  AbstractSqlDao
 */
public class MovieSqlDao extends AbstractSqlDao<Integer, Movie> {

    public MovieSqlDao(){
        super();
        beanType=Movie.class; // Set the bean type for this Dao
    }

    @Override
    // Parse a row from the ResultSet into a bean object
    protected Movie parseRow(ResultSet rs) throws SQLException {
        // "(title, year, duration, country, language, description, genre, image, cert)"
        Movie movie=new Movie();

        movie.setId(rs.getInt("id"));
        movie.setTitle(rs.getString("title"));
        movie.setYear(rs.getInt("year"));
        movie.setDuration(rs.getInt("duration"));
        movie.setCountry(rs.getString("country"));
        movie.setLanguage(rs.getString("language"));
        movie.setDescription(rs.getString("description"));
        movie.setGenre(rs.getString("genre"));
        movie.setImage(rs.getString("image"));
        movie.setCert(rs.getString("cert"));
        movie.setDirector(rs.getString("director"));
        movie.setStars(rs.getString("stars"));

        return movie;
    }


    @Override
    // create the statement for adding the bean Movie
    protected PreparedStatement addStatement(Connection connection, Movie bean) throws SQLException {
        final String SQL_ADD="INSERT INTO movie" +
                " (title, year, duration, country, language, description, genre, image, cert, director, stars)" +
                " VALUES (?,?,?,?,?,?,?,?,?,?,?);";

        PreparedStatement ps=connection.prepareStatement(SQL_ADD, Statement.RETURN_GENERATED_KEYS);

        ps.setString(1,bean.getTitle());
        ps.setInt(2,bean.getYear());
        ps.setInt(3,bean.getDuration());
        ps.setString(4,bean.getCountry());
        ps.setString(5,bean.getLanguage());
        ps.setString(6,bean.getDescription());
        ps.setString(7,bean.getGenre());
        ps.setString(8,bean.getImage());
        ps.setString(9,bean.getCert());
        ps.setString(10,bean.getDirector());
        ps.setString(11,bean.getStars());

        return ps;
    }

    // Create the update statement for the bean
    @Override
    protected PreparedStatement updateStatement(Connection connection, Movie bean) throws SQLException {
        final String SQL_UPDATE="UPDATE movie SET" +
                " title=?, year=?, duration=?, country=?, language=?, description=?," +
                " genre=?, image=?, cert=?, director=?, stars=? WHERE id=?;";

        PreparedStatement ps=connection.prepareStatement(SQL_UPDATE);

        ps.setString(1,bean.getTitle());
        ps.setInt(2,bean.getYear());
        ps.setInt(3,bean.getDuration());
        ps.setString(4,bean.getCountry());
        ps.setString(5,bean.getLanguage());
        ps.setString(6,bean.getDescription());
        ps.setString(7,bean.getGenre());
        ps.setString(8,bean.getImage());
        ps.setString(9,bean.getCert());
        ps.setString(10,bean.getDirector());
        ps.setString(11,bean.getStars());
        ps.setInt(12,bean.getId());

        return ps;
    }
}
