package agrechnev.practsql.dao.impl;

import agrechnev.practsql.dao.api.Dao;
import agrechnev.practsql.dsource.DSource;
import agrechnev.practsql.model.Entity;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oleksiy Grechnyev on 9/10/2016.
 * An abstract (part-way) implementation of the Dao interface using SQL
 */
public abstract class AbstractSqlDao<K extends Number, T extends Entity<K>> implements Dao<K, T> {
    // SQL statements templates

    // Select all rows (beans) from table %s
    private static final String SQL_GETALL = "SELECT * FROM %s;";
    // Select one row by id (primary key) from table %s
    private static final String SQL_GET = "SELECT * FROM %s WHERE id=?;";
    // Delete one row
    private static final String SQL_DELETE = "DELETE FROM %s WHERE id=?;";
    // Delete all data
    private static final String SQL_DELETE_ALL = "DELETE FROM %s;";
    // Update one row
    private static final String SQL_UPDATE = "UPDATE %s SET #1 WHERE id=?;";

    // Fields
    protected Class<T> beanType=null; // The type of bean (must equal T)

    // Constructor which requires T as the Class object
    public AbstractSqlDao(Class<T> beanType) {
        this.beanType = beanType;
    }

    // A constructor
    public AbstractSqlDao() {
        // Nothing at the moment, we get a new c3p0 connection every time fro DSource

// Old connection options for testing
//        connection= DSource.getInstance().getConnection();

/*        connection=DriverManager.getConnection("jdbc:mysql://localhost/theater",
                "agrechnev","Q1w2E3r4");*/
    }

    //------------------------------------------------------
    // METHODS

    // Get all beans from the database as a list
    @Override
    public List<T> getAll() throws DaoException {
        // Put the bean name into the SQL query
        List<T> list = new ArrayList<>(); // Create an empty ArrayList for the result
        String sqlQuery = String.format(SQL_GETALL, beanType.getSimpleName());

        try (Connection connection=DSource.getInstance().getConnection();
                Statement s = connection.createStatement();
             ResultSet rs = s.executeQuery(sqlQuery)) {

            while (rs.next()) {
                list.add(parseRow(rs)); // Parse a row into bean
            }

        } catch (SQLException e) {
            throwDaoEx("SQLException in AbstractSqlDao.getAll", e);
        } catch (DSource.DSourceException e) {
            throwDaoEx("DSourceException in AbstractSqlDao.getAll", e);
        }

        return list;
    }

    // Get a bean by id
    @Override
    public T get(K id) throws DaoException {
        // Put the bean name into the SQL query
        String sqlQuery = String.format(SQL_GET, beanType.getSimpleName());

        if (id == null) throwDaoEx("null id in AbstractSqlDao.get", null);


        T bean = null; // A bean to return

        try (Connection connection=DSource.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(sqlQuery)) {
            ps.setLong(1, id.longValue()); // This should handle both Integer and Long
            try (ResultSet rs = ps.executeQuery()) {
                if (!rs.next()) throwDaoEx("wrong id in AbstractSqlDao.get: id="+id, null);

                bean = parseRow(rs); // Get the result
            }

        } catch (SQLException e) {
            throwDaoEx("SQLException in AbstractSqlDao.get", e);
        } catch (DSource.DSourceException e) {
            throwDaoEx("DSourceException in AbstractSqlDao.get", e);
        }

        return bean; // A clean return outside of try{} as a good style

    }

    // Add a bean and return a correct id
    // Also sets ID in the bean object
    @Override
    public K add(T bean) throws DaoException {
        // Put the bean name into the SQL query
//        String sqlQuery = String.format(SQL_ADD, beanType.getSimpleName());

        if (bean == null) throwDaoEx("null bean in AbstractSqlDao.add", null);

        // Other parameters in the query
//        sqlQuery=sqlQuery.replaceAll("#1",bean.entityFieldNames())
//                         .replaceAll("#2",bean.entityFieldValues());


        try (Connection connection=DSource.getInstance().getConnection();
             PreparedStatement ps = addStatement(connection,bean)) {

            int result = ps.executeUpdate();

            if (result!=1) throwDaoEx("could not add record in AbstractSqlDao.add", null);

            // Update the bean with the new ID
            try(ResultSet rs=ps.getGeneratedKeys()){
                if (!rs.next()) throwDaoEx("cannot get id in AbstractSqlDao.add", null);
                bean.setId(createID(rs.getLong(1)));
            }


        } catch (SQLException e) {
            throwDaoEx("SQLException in AbstractSqlDao.add", e);
        } catch (DSource.DSourceException e) {
            throwDaoEx("DSourceException in AbstractSqlDao.add", e);
        }

        return bean.getId();
    }

    // Delete a bean by ID
    @Override
    public void delete(K id) throws DaoException {
        // Put the bean name into the SQL query
        String sqlQuery = String.format(SQL_DELETE, beanType.getSimpleName());

        if (id == null) throwDaoEx("null id in AbstractSqlDao.delete", null);

        try (Connection connection=DSource.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(sqlQuery)) {
            ps.setLong(1, id.longValue()); // Set the id value

            int result = ps.executeUpdate();

            if (result<1) throwDaoEx("could not delete record in AbstractSqlDao.delete: id="+id, null);


        } catch (SQLException e) {
            throwDaoEx("SQLException in AbstractSqlDao.delete", e);
        } catch (DSource.DSourceException e) {
            throwDaoEx("DSourceException in AbstractSqlDao.delete", e);
        }

    }

    // Delete all beans  (reset database)
    @Override
    public void deleteAll() throws DaoException {
        // Put the bean name into the SQL query
        String sqlQuery = String.format(SQL_DELETE_ALL, beanType.getSimpleName());

        try (Connection connection=DSource.getInstance().getConnection();
             Statement st = connection.createStatement()) {

            st.executeUpdate(sqlQuery);

        } catch (SQLException e) {
            throwDaoEx("SQLException in AbstractSqlDao.deleteAll", e);
        } catch (DSource.DSourceException e) {
            throwDaoEx("DSourceException in AbstractSqlDao.deleteAll", e);
        }
    }

    // Update a bean (must have a correct ID)
    @Override
    public void update(T bean) throws DaoException {

        if (bean == null) throwDaoEx("null bean in AbstractSqlDao.update", null);



        try (Connection connection=DSource.getInstance().getConnection();
             PreparedStatement ps = updateStatement(connection,bean)) {

            int result = ps.executeUpdate();

            if (result<1) throwDaoEx("could not update record in AbstractSqlDao.update: id="+bean.getId(), null);


        } catch (SQLException e) {
            throwDaoEx("SQLException in AbstractSqlDao.update", e);
        } catch (DSource.DSourceException e) {
            throwDaoEx("DSourceException in AbstractSqlDao.update", e);
        }

    }


    // Throw a DaoException with a text and cause
    private void throwDaoEx(String message, Throwable cause) throws DaoException {
        message += " : in class " + this.getClass();
        throw new DaoException(message, cause);
    }


    //---------------------------------------------------------------
    // Posibly overridden by children
    // Create the id of type K, as Long or Integer
    // Integer by default
    // We get a ClassCastException if we use a wrong type, I guess
    protected K createID(long id){
        return (K)(Integer)(int)id;     // Integer version
//        return (K)(Long)id;   // Long version
    };

    //---------------------------------------------------------------
    // Things to be implemented by all children

    // Create the add statement for the bean
    protected abstract PreparedStatement addStatement(Connection connection,  T bean) throws SQLException;

    // Create the update statement for the bean
    protected abstract PreparedStatement updateStatement(Connection connection,  T bean) throws SQLException;

    // Parse a row from the ResultSet into a bean object
    // Is allowed to throw SQLException if cannot parse
    protected abstract T parseRow(ResultSet rs) throws SQLException;

}
