package agrechnev.practsql.dao.impl;

import agrechnev.practsql.dao.api.Dao;
import agrechnev.practsql.model.Entity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Oleksiy Grechnyev on 8/12/2016.
 */
public class InMemDao<T extends Entity<Integer>> implements Dao<Integer, T>  {
    // Node of the list
    private static class Node<K extends Number,V extends Entity> {
        private K key;
        private V value;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public K getKey() {
            return key;
        }

        public void setKey(K key) {
            this.key = key;
        }

        public V getValue() {
            return value;
        }

        public void setValue(V value) {
            this.value = value;
        }
    }

    private ArrayList<Node<Integer, T>> data;
    private int currentId=0;

    public InMemDao() {
        data=new ArrayList<Node<Integer, T>>();
    }

    // Get the next ID
    public int nextId(){
        return ++currentId;
    }

    // Create a new record
    @Override
    public Integer add(T bean){
        int id=nextId();
        bean.setId(id);
        try {
            // We need to clone to avoid broken ids and stuff
            // To make behavior more database-like
            data.add(new Node<Integer,T>(id,(T)bean.clone()));
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return id;
    }

    @Override
    public void delete(Integer id) throws DaoException {
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getKey().equals(id)) {
                data.remove(i);
                return;
               };

        }
        throw new DaoException("InMemDao.delete : no record found with id="+id,null);
    }

    @Override
    // Delete all beans  (reset database)
    public void deleteAll() throws DaoException {
        data=null;
    }

    @Override
    public T get(Integer id) throws DaoException {
        for (Node<Integer, T> node: data){
            if (node.getKey().equals(id)) return node.getValue();
        };
        throw new DaoException("InMemDao.get : no record found with id="+id,null);

    }

    @Override
    public void update(T bean) throws DaoException {

        for (Node<Integer, T> node: data){
            if (node.getKey().equals(bean.getId())) {
                node.setValue(bean);
                return;
            };
        }
        throw new DaoException("InMemDao.update : no record found with id="+bean.getId(),null);
    }

    public int size() {
        return data.size();
    }

    // Get everything as a list
    @Override
    public List<T> getAll() {

        return data.stream().map(Node::getValue).collect(Collectors.toList());

    }
}
