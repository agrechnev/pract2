package agrechnev.practsql.dao.api;

import agrechnev.practsql.model.Entity;

import java.util.List;

/**
 * Created by Oleksiy Grechnyev on 9/10/2016.
 * Interface for a DAO classes which must implement CRUD
 * K is the key type (Integer or Long)
 * T is the bean type (i.e. Movie)
 */
public interface Dao<K extends Number,T extends Entity<K>> {
    static class DaoException extends Exception {
        public DaoException(String message, Throwable cause) {
            super(message, cause);
        }
    };


    // Get all beans as a list
    List<T> getAll() throws DaoException;

    // Get a bean by ID or null if not found
    T get(K id) throws DaoException;

    // The following methods cast DaoException if unsuccessfull

    // Delete a bean by ID
    void delete(K id) throws DaoException;

    // Delete all beans  (reset database)
    void deleteAll() throws DaoException;

    // Add a bean and return ID
    // Also sets ID in the bean object
    K add(T bean) throws DaoException;

    // Update a bean (must have a correct ID)
    void update(T bean) throws DaoException;

}
