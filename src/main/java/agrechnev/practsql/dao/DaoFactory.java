package agrechnev.practsql.dao;

import agrechnev.practsql.dao.api.Dao;
import agrechnev.practsql.dao.impl.InMemDao;
import agrechnev.practsql.dao.impl.MovieSqlDao;
import agrechnev.practsql.helpers.TheaterConfig;
import agrechnev.practsql.model.Movie;

/**
 * Created by Oleksiy Grechnyev on 9/10/2016.
 * A singleton class which provides DAOs for all beans
 * As either SQL or inMemoryDB
 */
public class DaoFactory {
    private static DaoFactory daoFactoryInstance=null; // The singleton instance

    // The DAOs
    private Dao<Integer,Movie> movieDao;


    private DaoFactory() {
        initDao();
    }

    private void initDao() {
        if (TheaterConfig.getInstance().getInMemoryDB()) {
            // Use InMemoryDB
            movieDao=new InMemDao<Movie>();
        } else {
            // Use SQL
            movieDao=new MovieSqlDao();
        }
    }

    public static DaoFactory getInstance()  {
        if (daoFactoryInstance == null) daoFactoryInstance=new DaoFactory();
        return daoFactoryInstance;
    }

    public Dao<Integer, Movie> getMovieDao() {
        return movieDao;
    }
}
