package agrechnev.pract4;

import java.util.*;

/**
 * Created by Oleksiy Grechnyev on 8/19/2016.
 */
public class StudentUtils {
    private StudentUtils() { } // Cannot be instantiated

    public static Map<String,Student> createMapFromList(List<Student> students) {
        HashMap<String,Student> map=new  HashMap<>();

        students.stream().forEach(s->{
            map.put(s.getFirstName()+' '+s.getLastName(),s);
        });

        return map;
    }

    // Print all students for a given year using iterator
    public static void printStudents(List<Student> students, int year){
        Iterator<Student> iter=students.iterator();

        System.out.println("---------------------");
        System.out.println("Students of the year"+year);
        System.out.println("---------------------");

        while (iter.hasNext()) {
            Student s=iter.next();
            if (s.getYear()==year) System.out.println(s);
        }
        System.out.println("---------------------");
    }

    // Sort the list of students by First Name
    public static List<Student> sortStudents(List<Student> students) {
        List<Student> sortedList=new ArrayList<Student>(students); // Clone the original list

        // Sort the list
        Collections.sort(sortedList,(s1,s2) -> s1.getFirstName().compareTo(s2.getFirstName()));

        return sortedList;
    }
}
