package agrechnev.pract4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import static agrechnev.pract4.DictUtils.SordDir.ACC;

/**
 * Created by Oleksiy Grechnyev on 8/19/2016.
 * Read a text from file and then analyze the number of word encounters
 */
public class DictUtils {
    private DictUtils() { }  // no-no

    enum SortBy {WORD,FREQ};
    enum SordDir {ACC,DEC};

    // read file as a string
    public static String readFile(String fileName) throws IOException{
        BufferedReader reader=new BufferedReader(new FileReader(fileName)); // Open file

        StringBuilder sb=new StringBuilder();

        String ls=System.getProperty("line.separator"); // Correct line separator
        String line;

        while ((line=reader.readLine()) != null) {
            sb.append(line); // Add line
            sb.append(ls); // Add separator
        }

        return sb.toString();
    }

    // Count words in a text
    public static Map<String,Integer> countWords(String text) {
        Map<String,Integer> map=new HashMap<>();

        Arrays.stream(text.split("\\W+")).forEach(s->{
//            System.out.println(s);
            if (map.containsKey(s)) { // Word already present
                map.replace(s,map.get(s)+1); // Increase count by 1
            } else {
                map.put(s,1); // First time encountered this word
            }
        });

        return map;
    }

    // Count words in a text and sort
    // Sort by by in direction dir
    public static Map<String,Integer> countWordsSort(String text, SortBy by, SordDir dir) {

        // Get the unsorted Map and transform it to ArrayList of entries
        // using the unsorted version of countWords
        List<Map.Entry<String,Integer>> entryList=new ArrayList<Map.Entry<String,Integer>>(
                countWords(text).entrySet()
        );

        // Create the comparator
        Comparator<Map.Entry<String,Integer>> comparator=null;
        switch (by) {
            case WORD:
                comparator=(e1,e2) -> {
                    return (dir==ACC ? 1 : -1)*e1.getKey().compareTo(e2.getKey());
                };
                break;
            case FREQ:
                comparator=(e1,e2) -> {
                    return (dir==ACC ? 1 : -1)*Integer.compare(e1.getValue(),e2.getValue());
                };
                break;
        }

        // Sort the list using the external comparator
        Collections.sort(entryList,comparator);

        // Create  a LinkedHashMap object from the sorted list
        Map<String,Integer> map=new LinkedHashMap<String,Integer>();

        entryList.stream().forEach(e-> map.put(e.getKey(),e.getValue()) );

        return map;
    }
}
