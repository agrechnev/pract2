package agrechnev.pract4;

import java.io.IOException;
import java.util.Map;

/**
 * Created by Oleksiy Grechnyev on 8/19/2016.
 */
public class DictUtilsdemo {
    public static void main(String[] args) throws IOException {
        String text1=DictUtils.readFile("Romeo.txt");

        String text2="John Mary Smith\nJohn\r\n\r\nRoger Mary Mary John Leonard"; // Simpler text
        String text3="John Mary Smith John Roger Mary Mary John   Leonard"; // Simpler text

//        System.out.println(text);
        System.out.println("Map :");

        Map<String,Integer> map=DictUtils.countWords(text1); // Get the map

        // Print the map
//        map.keySet().stream().forEach(s-> System.out.println(s+':'+map.get(s)));


        // Get the sorted map
        Map<String,Integer> sMap=DictUtils.countWordsSort(text1, DictUtils.SortBy.WORD, DictUtils.SordDir.ACC);

        // Print the sorted map
        System.out.println("----------");
        System.out.println("Sorted map:");
        sMap.keySet().stream().forEach(s-> System.out.println(s+':'+sMap.get(s)));


    }
}
