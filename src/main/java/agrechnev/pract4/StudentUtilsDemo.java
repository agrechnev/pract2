package agrechnev.pract4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by Oleksiy Grechnyev on 8/19/2016.
 */
public class StudentUtilsDemo {
    public static void main(String[] args) {
        List<Student> students=new ArrayList<Student>(Arrays.asList(
                new Student("Tom","Riddle",1),
                new Student("Seymour","Guado",4),
                new Student("Jon","Irenicus",1),
                new Student("Safer","Sephiroth",2),
                new Student("Darth","Malak",5),
                new Student("Darth","Nihilus",3),
                new Student("Saren","Arterius",5),
                new Student("Kuja","Unknown",2),
                new Student("Yu","Yevon",4),
                new Student("Cid","Raines",3),
                new Student("Vayne","Solidor",5)
        ));

        System.out.println("------------------");
        System.out.println("The list:");
        students.stream().forEach(System.out::println); // Print the list

        // Create and print the map
        Map<String,Student> map= StudentUtils.createMapFromList(students);
        System.out.println("------------------");
        System.out.println("The map:");
        map.keySet().forEach(k->{
            System.out.println(k+':'+map.get(k));
        });

        // Print students of each year using printStudents
        System.out.println("------------------");
        for (int i = 1; i <= 5; i++) StudentUtils.printStudents(students,i);

        // Create a sorted student list
        List<Student> sortedList=StudentUtils.sortStudents(students);
        System.out.println("------------------");
        System.out.println("Sorted students:");

        sortedList.stream().forEach(System.out::println); // Print the list
    }
}
