package agrechnev.pract3;

/**
 * Created by Oleksiy Grechnyev on 8/12/2016.
 */
public class Movie extends Entity<Integer>{
    private String title; // Movie title
    private int year;  // Movie relese year
    private int duration;   // Duration in min
    private String country;
    private String language;
    private String description;
    private String genre;
    private String image; // Movie poster file or URL
    private String cert;  // Movie certification

    public Movie(String title, int year, int duration, String country, String language, String description, String genre, String image, String cert) {
        this.title = title;
        this.year = year;
        this.duration = duration;
        this.country = country;
        this.language = language;
        this.description = description;
        this.genre = genre;
        this.image = image;
        this.cert = cert;
    }

    public Movie() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCert() {
        return cert;
    }

    public void setCert(String cert) {
        this.cert = cert;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", year=" + year +
                ", duration=" + duration +
                ", country='" + country + '\'' +
                ", language='" + language + '\'' +
                ", description='" + description + '\'' +
                ", genre='" + genre + '\'' +
                ", image='" + image + '\'' +
                ", cert='" + cert + '\'' +
                '}';
    }


}
