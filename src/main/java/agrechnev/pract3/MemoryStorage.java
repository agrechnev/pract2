package agrechnev.pract3;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oleksiy Grechnyev on 8/12/2016.
 */
public class MemoryStorage<V extends Entity> implements GenericStorage<Long,V> {
    // Node of the list
    private static class Node<K extends Number,V extends Entity> {
        private K key;
        private V value;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public K getKey() {
            return key;
        }

        public void setKey(K key) {
            this.key = key;
        }

        public V getValue() {
            return value;
        }

        public void setValue(V value) {
            this.value = value;
        }
    }

    private ArrayList<Node<Long,V>> data;
    private long currentId;

    public MemoryStorage() {
        data=new ArrayList<Node<Long, V>>();
        currentId=0;
    }

    // Get the next ID
    public long nextId(){
        return ++currentId;
    }

    // Create a new record


    public Long create(V value) {
        long key=nextId();
        value.setId(key);
        data.add(new Node(key,value));
        return key;
    }

    public boolean delete(Long key) {
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getKey()==key) {
                data.remove(i);
                return true;
            };

        }
        return false;
    }

    public V read(Long key) {
        for (Node<Long,V> node: data){
            if (node.getKey()==key) return node.getValue();
        };
        return null;

    }

    public boolean update(Long key, V newValue) {
        for (Node<Long,V> node: data){
            if (node.getKey()==key) {
                node.setValue(newValue);
                return true;
            };
        }
        return false;
    }

    public long size() {
        return data.size();
    }

    public List<V> readAll() {
        List<V> result=new ArrayList<V>();
        for (Node<Long,V> node: data){
            result.add(node.getValue());
        }
        return result;
    }
}
