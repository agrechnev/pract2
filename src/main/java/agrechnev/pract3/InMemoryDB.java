package agrechnev.pract3;

/**
 * Created by Oleksiy Grechnyev on 8/12/2016.
 */
public class InMemoryDB {
    public static MemoryStorage<User> userStorage=new MemoryStorage<>();
    public static MemoryStorage<Movie> movieStorage=new MemoryStorage<>();
}
