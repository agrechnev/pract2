package agrechnev.pract3;

import java.util.List;

/**
 * Created by Oleksiy Grechnyev on 8/12/2016.
 */
public interface GenericStorage<K,V extends Entity> {
    public K create(V value);
    public boolean delete(K key);
    public V read(K key);
    public boolean update(K key, V newValue);
    public long size();
    public List<V> readAll();
}
