package agrechnev.pract3;

import java.util.List;

/**
 * User of
 * Created by Oleksiy Grechnyev on 8/12/2016.
 */
public class User extends  Entity{
    public enum UserRole {ADMIN, USER};

    private UserRole role;
    private String login;
    private String email;
    private String fName;
    private String lName;

    private List<Ticket> ticketList;

    public User() {
    }

    public User(long id, UserRole role, String login, String email, String fName, String lName, List<Ticket> ticketList) {
        super(id);
        this.role = role;
        this.login = login;
        this.email = email;
        this.fName = fName;
        this.lName = lName;
        this.ticketList = ticketList;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public List<Ticket> getTicketList() {
        return ticketList;
    }

    public void setTicketList(List<Ticket> ticketList) {
        this.ticketList = ticketList;
    }

    @Override
    public String toString() {
        return "User{" +
                "role=" + role +
                ", login='" + login + '\'' +
                ", email='" + email + '\'' +
                ", fName='" + fName + '\'' +
                ", lName='" + lName + '\'' +
                ", ticketList=" + ticketList +
                '}';
    }
}
