package agrechnev.pract3;

import java.io.Serializable;

/**
 * Created by Oleksiy Grechnyev on 8/12/2016.
 */
public abstract class Entity<T> implements Serializable {
   private T id;

    public Entity() {
    }

    public Entity(T id) {
        this.id = id;
    }

    public T getId() {
        return id;
    }

    public void setId(T id) {
        this.id = id;
    }
}
