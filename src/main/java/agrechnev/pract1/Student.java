package agrechnev.pract1;

import java.util.ArrayList;

/**
 * Created by student on 7/12/2016.
 */
public class Student {

    public static class StudentException extends Exception {}

    public static class Exam{
        private String subject;
        private int grade;
        private int year;
        private int term;

        public Exam(String subject, int grade, int year, int term) {
            this.subject = subject;
            this.grade = grade;
            this.year = year;
            this.term = term;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public int getGrade() {
            return grade;
        }

        public void setGrade(int grade) {
            this.grade = grade;
        }

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }

        public int getTerm() {
            return term;
        }

        public void setTerm(int term) {
            this.term = term;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Exam exam = (Exam) o;

            if (grade != exam.grade) return false;
            if (year != exam.year) return false;
            if (term != exam.term) return false;
            return subject.equals(exam.subject);

        }

        @Override
        public int hashCode() {
            int result = subject.hashCode();
            result = 31 * result + grade;
            result = 31 * result + year;
            result = 31 * result + term;
            return result;
        }
    }

    public static class Group{
        int year;
        String faculty;

        public Group(int year, String faculty) {
            this.year = year;
            this.faculty = faculty;
        }

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }

        public String getFaculty() {
            return faculty;
        }

        public void setFaculty(String faculty) {
            this.faculty = faculty;
        }
    }

    //----------------------
    private String firstName;
    private String familyName;
    private Group group;

    private ArrayList<Exam> exams;

    public Student(String firstName, String familyName, Group group) {
        this.firstName = firstName;
        this.familyName = familyName;
        this.group = group;
        this.exams=new ArrayList<Exam>();
    }


    public Student(String firstName, String familyName, Group group, ArrayList<Exam> exams) {
        this.firstName = firstName;
        this.familyName = familyName;
        this.group = group;
        this.exams=exams;
    }

    // Add the exam e
    public void addExam(Exam e){
        for (Exam e1:exams) {
            if (e.equals(e1)) return; // Already there
        }

        exams.add(e);
    }

    // Remove exam e
    public void removeExam(Exam e) throws StudentException {
        for (int index=0;index< exams.size();index++) {
            if (exams.get(index).equals(e)) {
                exams.remove(index);
                return;
            }
        }

        // Not found
        throw new StudentException();
    }

    // Number of exams with grade
    public int numWithGrade(int grade){
        int result=0;

        for (int index=0;index< exams.size();index++) {
            if (exams.get(index).getGrade()==grade)   result++;

        }

        return result;
    }

    // Average grade for year, term
    public double averageGrade(int year, int term){
        double result=0;
        int counter=0;

        for (int index=0;index< exams.size();index++) {
            Exam e=exams.get(index);
            if (e.getYear()==year && e.getTerm()==term) {
              counter++;
              result=result+e.getGrade();
            }
        }

        if (counter==0) {
            return 0.0d;
        } else {
            return result/counter;
        }
    }

    // highest grade for a subject
    int highestGrade(String subject) {
        int highest=0;

        for (int index=0;index< exams.size();index++) {
            Exam e=exams.get(index);
            if (e.getSubject().equals(subject)) {
                if (e.getGrade() > highest || index==0) highest=e.getGrade();
            }
        }

        return highest;
    }
}
