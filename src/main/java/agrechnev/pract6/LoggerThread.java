package agrechnev.pract6;

import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.Spliterator;

/**
 * Created by Oleksiy Grechnyev on 9/7/2016.
 * Log all files from a Spliterator which match regex
 */
public class LoggerThread extends Thread {
    private Spliterator<Path> fileSplit;
    private String regex;
    private PrintWriter log;

    public LoggerThread(Spliterator<Path> fileSplit, String regex, PrintWriter log) {
        this.fileSplit = fileSplit;
        this.regex = regex;
        this.log = log;
    }

    @Override
    public void run() {
        System.out.println("starting thread "+this);
        if (fileSplit == null) return;

        fileSplit.forEachRemaining(p->{
            //System.out.println(this+":"+p);
            if (p.getFileName().toString().matches(regex)) {
                // Synchronize the writing to log just in case
                // Although it is already done in PrintWriter, not documented though
                synchronized (log) {
                    log.println(p);
                }
            }
        });
    }
}
