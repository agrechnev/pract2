package agrechnev.pract6;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Spliterator;
import java.util.stream.Collectors;

/**
 * Created by Oleksiy Grechnyev on 9/7/2016.
 * Find files and write them to a log using threads
 */
public class FileLogger {
    private FileLogger() {throw new Error(); }

    // Log files which names match regex from the dir into log
    public static void logFiles(Path dir, String regex, PrintWriter log){
        // Get the list of files
        List<Path> fileList= null;
        try {
            fileList = Files.list(dir).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        // Split the list into two smaller lists
        // Just a demo, split into many parts would be nicer for a real parallel app

        Spliterator<Path> part1 =fileList.spliterator();
        Spliterator<Path> part2 =part1.trySplit();

        // Create threads
        Thread thread1=new LoggerThread(part1,regex,log);
        Thread thread2=new LoggerThread(part2,regex,log);

        // Start threads
        thread1.start();
        thread2.start();

        // Wait for the threads
        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {

        try (PrintWriter logFile=new PrintWriter(new BufferedWriter(new FileWriter("data6\\log.txt")))) {
            logFiles(Paths.get("data6\\s"), ".+\\.txt", logFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
