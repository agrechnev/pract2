package agrechnev.pract6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Oleksiy Grechnyev on 9/7/2016.
 * Copy all files from a directory in parallel using CopierThread
 */
public final class ParaCopy {
    private ParaCopy() {throw new Error(); }

    // Copy all files from directory source to directory dest
    public static void copyAll(Path source, Path dest){

        // Check the arguments
        if (!Files.exists(source) || !Files.isDirectory(source)) {
            System.out.println("Error: directory "+source+" does not exist!");
            return;
        }

        if (!Files.exists(dest) || !Files.isDirectory(dest)) {
            System.out.println("Error: directory "+dest+" does not exist!");
            return;
        }

        try { // Check if source and dest is the same dir
            if (Files.isSameFile(source,dest)) {
                System.out.println("Error: "+source+" and  "+dest+" is the same directory!");
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        // Get the file list in source
        try {
            // Create the thread list
            List<Thread> threads=Files.list(source).map(p->new CopierThread(p,dest))
                    .collect(Collectors.toList());

            // Start the threads
            threads.forEach(Thread::start);

            // Wait for the threads to finish
            threads.forEach((t) -> {
                try {
                    t.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
    }

    public static void main(String[] args) throws IOException {
        BufferedReader in =new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter source:");
        String source=in.readLine();

        System.out.println("Enter destination:");
        String dest=in.readLine();

        copyAll(Paths.get(source),Paths.get(dest)); // Run the copy
    }
}
