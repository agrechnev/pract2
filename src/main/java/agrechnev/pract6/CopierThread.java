package agrechnev.pract6;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

/**
 * Created by Oleksiy Grechnyev on 9/7/2016.
 * A thread to copy a file to destDir
 */
public class CopierThread extends Thread{
    private Path file; // File to ocpy
    private Path destDir; // Destination directory

    public CopierThread(Path file, Path destDir) {
        super(); // Just for clairty, can be omitted
        this.file = file;
        this.destDir = destDir;
    }

    @Override
    public void run() {
        Path dest=destDir.resolve(file.getFileName()); // Destination name

        System.out.println("Copying "+file+" to "+dest);

        try {
            Files.copy(file,dest,REPLACE_EXISTING);
        } catch (IOException e) {
            System.out.println("Error: could not copy file "+file);
            System.out.println("to "+dest);
        }
    }
}
