package agrechnev.pract6;

/**
 * Created by Oleksiy Grechnyev on 9/6/2016.
 * Create a thread which prints its name every 0.5 seconds for 5 seconds
 */
public class NameWriter5 {

    // The actual logic of the thread
    private static void writeName(){
        String name=Thread.currentThread().getName(); // Thread name

        int count=11; // Print name exactly 11 times every 0.5 sec, then quit
        long oldTime=System.currentTimeMillis() / 500; // The old time in half-sec
        long newTime; // The new one

        while (count>0){
            newTime=System.currentTimeMillis() / 500;
            if (newTime!=oldTime) { // Half-second changed
                System.out.println(name);
                count--;
            }
            oldTime=newTime; // Save the old time
            nap(10); // Nap for a little while
        }

    }

    // Sleep and ignore InterruptedException
    private static void nap(int sleepTime) {
        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // main: run the thread in 2 ways
    public static void main(String[] args) throws InterruptedException {

        System.out.println("Using anonymous extension of Thread:");
        Thread thread1=new Thread("thread1"){
            @Override
            public void run() {
                writeName();
            }
        };

        thread1.start();
        thread1.join(); //Wait for it to finish

        System.out.println("Using Runnable lambda expression:");
        Thread thread2=new Thread(()->writeName(), "thread2");

        thread2.start();
    }
}
