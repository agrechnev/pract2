package agrechnev.pract6;

import java.util.*;

/**
 * Created by Oleksiy Grechnyev on 9/6/2016.
 * Get a Map of time stamps (ms) + messages and print them
 */
public class MySchedule {
    private MySchedule() {
        throw new Error();
    }

    // Print the schedule
    public static void printSchedule(Map<Long, String> schedule) {

        // Create a sorted key set
        final Set<Long> keys = new TreeSet<>(schedule.keySet());
        final int timeQuantum = 50; // The sleep time between checks

        // Create the thread with a Runnable lambda
        new Thread(() -> {
            final long startTime = System.currentTimeMillis(); // Get the starting time in ms

            // Iterate over keys in the sorted keyset
            keys.forEach(t -> {  // Long t = time
                // Sleep until it's time

                long time; // The current time

                // Wait until it's time
                for (;;) {
                    time = System.currentTimeMillis();
                    if ((time - startTime) > t) break; // It's time
                    nap(timeQuantum); // Nap for a short while
                }

                //Print the message
                System.out.println(schedule.get(t));
            });

        }).start();

    }

    // Sleep and ignore InterruptedException
    private static void nap(int sleepTime) {
        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // Test our method
    public static void main(String[] args) {
        Map<Long, String> timeTable = new HashMap<>();
        timeTable.put(2000L, "Mark 2000");
        timeTable.put(500L, "Mark 500");
        timeTable.put(5500L, "Mark 5500");
        timeTable.put(2500L, "Mark 2500");


        printSchedule(timeTable);
    }
}
