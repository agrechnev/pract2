package agrechnev.pract5;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by Oleksiy Grechnyev on 8/30/2016.
 *  Create a file of random integers, than read it
 */
public class RandomIntFile {

    // Create a text file of random ints
    public static void createRandFile(String fileName, int bound, int numInts) {
        try (PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(fileName)))) {
            Random random = new Random();

            for (int i = 0; i < numInts; i++) {
                pw.format("%d%n",random.nextInt(bound));
            }

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("IOException caught in createRandFile !");
        }
    }

    // Read random file and output sorted to stdout
    public static void readRandFile(String fileName){
        try {
            List<String> sList=StringReader.readFile(fileName);

            List<Integer> iList=sList.stream().map(Integer::parseInt).collect(Collectors.toList());

            Collections.sort(iList);
            iList.forEach(System.out::println);

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("IOException caught in readRandFile !");
        }
    }

    public static void main(String[] args) {
        createRandFile("rand.txt",1000,10);
        readRandFile("rand.txt");
    }
}
