package agrechnev.pract5;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Oleksiy Grechnyev on 8/30/2016.
 * Read students from text file and analyze an average grade
 */
public class StudentGradeReader {
    private StudentGradeReader() {throw new Error(); }

    private static class MyStudent{
        String name;
        List<Integer> grades;

        public MyStudent(String name) {
            this.name = name;
            grades=new ArrayList<Integer>();
        }
    }
    private static class MyStudent2{
        String name;
        int grade;

        public MyStudent2(String name, int grade) {
            this.name = name;
            this.grade = grade;
        }
    }

    public static void readGrades(String fileName) throws IOException {
        List<String> sList=StringReader.readFile(fileName); // Read the strings

        Map<String,MyStudent> map=new HashMap<>(); // Hashmap for students

        for (String line : sList) {
            MyStudent2 ms=getStudent(line);
            if (ms != null) { // Add this student to the hashmap

                // Create a new record if not found
                if (!map.keySet().contains(ms.name)) map.put(ms.name,new MyStudent(ms.name));
                map.get(ms.name).grades.add(ms.grade); // Add the grade
            }
        }


        // Now let us print the average grade if >90
        for (Map.Entry<String,MyStudent> en:map.entrySet()) {
            double avgGrade=0; // Average grade
            for (Integer i: en.getValue().grades) avgGrade+=i;

            avgGrade/=en.getValue().grades.size(); // Divide by # of grades

            if (avgGrade>90.0) {
                System.out.println(en.getKey()+":"+avgGrade);
            }
        }
    }

    // Parse the line with a single student or return 0 if unsuccessful
    private static MyStudent2 getStudent(String line) {
        Matcher m= Pattern.compile("([^=]+?)\\s*=\\s*(\\d+)").matcher(line);

        if (!m.matches()) return null; // No match
        return new MyStudent2(m.group(1),Integer.parseInt(m.group(2)));
    }

    public static void main(String[] args) throws IOException {
        readGrades("grades.txt");
    }
}
