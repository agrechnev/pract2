package agrechnev.pract5;

import java.io.Serializable;

/**
 * Created by Oleksiy Grechnyev on 9/1/2016.
 * A simple student for serialization
 */
public class Student implements Serializable {
    private static final long serialVersionUID = -3676417123467470575L;
    private String firstName;
    private String lastName;
    private int year;



    public Student(String firstName, String lastName, int year) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.year = year;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Student{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", year=" + year +
                '}';
    }
}
