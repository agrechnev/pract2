package agrechnev.pract5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Created by Oleksiy Grechnyev on 8/30/2016.
 * Read a text file as an ArrayList of Strings
 */
public class StringReader {
    private StringReader() { throw new Error();
    }

    public static ArrayList<String> readFile(String fileName) throws IOException {
        try (BufferedReader br= new BufferedReader(new FileReader(fileName))) {

            ArrayList<String> al = new ArrayList<>();

            String line;
            while ((line = br.readLine()) != null) al.add(line);

            return al;

        }
    }
}
