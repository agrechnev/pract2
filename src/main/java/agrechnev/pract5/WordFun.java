package agrechnev.pract5;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Oleksiy Grechnyev on 8/30/2016.
 * Swap first and last words in a line
 * Do the same to each line of a file
 * Do the same to each sentence in a file
 */
public class WordFun {
    private WordFun() {throw new Error(); }


    // Swap words of each line of file
    public static void swapWordsInLine(String fileName) throws IOException {
        List<String> sList=StringReader.readFile(fileName);

        for (String line : sList) {
            System.out.println(swapWords(line));
        }
    }

    //Swap words of each sentence of file
    public static void swapWordsInSentence(String fileName) throws IOException {
        List<String> sList=StringReader.readFile(fileName);

        // Now join the strings
        StringBuilder sb=new StringBuilder();

        sList.forEach(s->sb.append(s).append(" "));

        // Split it with '.' character and run through with swapWords
        for (String line : sb.toString().split("\\.")) {
            System.out.println(swapWords(line)+'.');
        }
    }

    // Swap first and last word
    public static String swapWords(String line){
        Matcher m= Pattern.compile("(\\W*)(\\w+)\\b(.+?)\\b(\\w+)(\\W*)",Pattern.MULTILINE).matcher(line);

        if (!m.matches()) {
            return line; // Not fiound
        } else {
            StringBuilder sb=new StringBuilder(m.group(1));
            sb.append(m.group(4));
            sb.append(m.group(3));
            sb.append(m.group(2));
            sb.append(m.group(5));
            return sb.toString();
        }
    }

    public static void main(String[] args) throws IOException {
        swapWordsInSentence("Romeo.txt");
//        System.out.println(swapWords("oneword"));
    }
}
