package agrechnev.pract5;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Oleksiy Grechnyev on 8/30/2016.
 * Benchmark copying files with different means
 */
public class CopyBench {
    private CopyBench() {throw new Error(); }

    interface Copier{
        void run(String s1,String s2) throws IOException;
    }



    // Copy with a non-buffered character stream
    public static void copyC(String fileName1, String fileName2) throws IOException {
        try (FileReader in=new FileReader(fileName1);
            FileWriter out=new FileWriter(fileName2)) {

         int i;

         while ((i=in.read())!=-1) out.write(i);
        }
    }

    // Copy with a buffered character stream
    public static void copyCB(String fileName1, String fileName2) throws IOException {
        try (BufferedReader in=new BufferedReader(new FileReader(fileName1));
        PrintWriter out=new PrintWriter(new BufferedWriter(new FileWriter(fileName2)))) {

         String s;

         while  ((s=in.readLine())!=null) out.println(s);
        }
    }


    // Copy with a non-buffered byte stream
    public static void copyB(String fileName1, String fileName2) throws IOException {
        try (FileInputStream in=new FileInputStream(fileName1);
        FileOutputStream out= new FileOutputStream(fileName2)) {

            int i;

            while ((i = in.read()) != -1) out.write(i);
        }
    }


    // Copy with a buffered byte stream
    public static void copyBB(String fileName1, String fileName2) throws IOException {
        try (BufferedInputStream in=new BufferedInputStream(new FileInputStream(fileName1));
        BufferedOutputStream out=new BufferedOutputStream(new FileOutputStream(fileName2))) {

            int i;

            while ((i = in.read()) != -1) out.write(i);
        }
    }

    // Copy with NIO
    public static void copyNIO(String fileName1, String fileName2) throws IOException {
        Path p1= Paths.get(fileName1);
        Path p2= Paths.get(fileName2);

        Files.deleteIfExists(p2);
        Files.copy(p1,p2);
    }

    // Benchmark a copy method
    public static long bench(Copier copier) throws IOException {
        long starttime=System.currentTimeMillis();
        for (int i = 0; i < 100; i++) {
            copier.run("Romeo.txt","copy.txt");
        }
        long endtime=System.currentTimeMillis();

        return endtime-starttime;
    }

    public static void main(String[] args) throws IOException {
        // Warm up the java machine

        int result=0;
        for (int i = 0; i < 100000000; i++) {
            result=(result+i)%1001;
        }

//        copyNIO("Romeo.txt","copy.txt");
//        System.exit(0);

        System.out.println("Starting the benchmark test !");
        System.out.println("copyC="+bench(CopyBench::copyC));
        System.out.println("copyCB="+bench(CopyBench::copyCB));
        System.out.println("copyB="+bench(CopyBench::copyB));
        System.out.println("copyBB="+bench(CopyBench::copyBB));
        System.out.println("copyNIO="+bench(CopyBench::copyNIO));

    }
}
