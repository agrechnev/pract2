package agrechnev.pract5;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Oleksiy Grechnyev on 9/6/2016.
 * A simple serialization example
 * Create a collection of students, save it, then load
 */
public class StudentIO {
    public static void main(String[] args) throws IOException, ClassNotFoundException {

        // Create a sample list of Students
        List<Student> sList = new ArrayList<Student>(Arrays.asList(
                new Student("Seymour", "Guado", 1),
                new Student("Sauron", "Maia", 2),
                new Student("Saren", "Arterius", 5),
                new Student("Sebastien", "Lacroix", 4),
                new Student("Jon", "Irenicus", 3),
                new Student("Darth", "Sion", 2),
                new Student("Darth", "Malak", 1),
                new Student("Bob", "Page", 4),
                new Student("Jerec", "Miraluka", 5)
        ));

        //Save it to a file
        try (ObjectOutputStream out=new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("student.bin")))) {
            out.writeObject(sList);
        }

        // Destroy the original list
        sList=null;
        System.gc();

        // Now read it from file
        List<Student> newList;

        try (ObjectInputStream in=new ObjectInputStream(new BufferedInputStream(new FileInputStream("student.bin")))){
            newList= (List<Student>) in.readObject(); // Read the list
        }

        // Print the list
        newList.forEach(System.out::println);
    }
}
