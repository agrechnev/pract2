package agrechnev.pract2;

import java.io.*;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Lenovo on 8/5/2016.
 */
public class NoteParse {
    public static class Notebook{
        private String manufacturer;
        private String  model;
        private String description;
        private int price;

        public Notebook() {
        }

        public Notebook(String manufacturer, String model, String description, int price) {
            this.manufacturer = manufacturer;
            this.model = model;
            this.description = description;
            this.price = price;
        }

        public String getManufacturer() {
            return manufacturer;
        }

        public void setManufacturer(String manufacturer) {
            this.manufacturer = manufacturer;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        @Override
        public String toString() {
            return "Notebook{" +
                    "manufacturer='" + manufacturer + '\'' +
                    ", model='" + model + '\'' +
                    ", description='" + description + '\'' +
                    ", price=" + price +
                    '}';
        }
    }

    private String text;

    public  NoteParse(String fileName) {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName),"windows-1251"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }

        String s = null;

        StringBuilder sb = new StringBuilder();

        try {
            while ((s = br.readLine()) != null) {

                sb.append(s).append(System.getProperty("line.separator"));

            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        text=sb.toString();
    }

    public ArrayList<Notebook> parseNoteBook(){

        ArrayList<Notebook> list=new ArrayList<Notebook>();

        Pattern p=Pattern.compile("\"lightbox\" title=\"Ноутбук (\\w+) ([^\"]+)\""+
        ".+?class=\"description\">([^<>]+)<br />.+?cost\">(\\d+) грн",Pattern.DOTALL);
        Matcher m=p.matcher(text);
        while (m.find()) {
            Notebook nb=new Notebook();

            nb.setManufacturer(m.group(1));
            nb.setModel(m.group(2));
            nb.setDescription(m.group(3));
            nb.setPrice(Integer.parseInt(m.group(4)));
            list.add(nb);
        }

        return list;
    }

    public static void main(String[] args) {
        NoteParse np=new NoteParse("source.html");

        ArrayList<Notebook> list=np.parseNoteBook();

        for (Notebook nb: list) {
            System.out.println(nb);
        }
    }
}
