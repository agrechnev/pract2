package agrechnev.pract2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Lenovo on 8/5/2016.
 */
public class BabyParse {
    // Pair of names, used as list element
    public static class BabyPair{
        private int number;
        private String male;
        private String female;

        public int getNumber() {
            return number;
        }

        public String getMale() {
            return male;
        }

        public String getFemale() {
            return female;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public void setMale(String male) {
            this.male = male;
        }

        public void setFemale(String female) {
            this.female = female;
        }

        // Constructor
        public BabyPair(int number, String male, String female) {
            this.number = number;
            this.male = male;
            this.female = female;
        }

        public BabyPair() {
        }

        @Override
        public String toString() {
            return "BabyPair{" +
                    "number=" + number +
                    ", male='" + male + '\'' +
                    ", female='" + female + '\'' +
                    '}';
        }
    }

    public static ArrayList<BabyPair> parseFile(String filename) {
        File file = new File(filename);
        ArrayList<BabyPair> result=new ArrayList<BabyPair>();

        // The regex
        Pattern p=Pattern.compile("<td>(\\d+)<\\/td><td>(\\w+)<\\/td><td>(\\w+)<\\/td>");

        try {

            Scanner sc = new Scanner(file);

            while (sc.hasNextLine()) {
                String  s = sc.nextLine();
                // Make the match
                Matcher m=p.matcher(s);

                if (m.find()) {
                    BabyPair pair=new BabyPair();

                    pair.setNumber(Integer.parseInt(m.group(1)));
                    pair.setMale(m.group(2));
                    pair.setFemale(m.group(3));
                    result.add(pair);
                }
            }
            sc.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static void main(String[] args) {

        System.out.println("This is the baby name parser !");

        ArrayList<BabyPair> result=parseFile("baby2008.html");

        for (BabyPair bp: result) {
            System.out.println(bp);
        }
    }
}
